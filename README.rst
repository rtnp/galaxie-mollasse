================
Galaxie Mollasse
================

The Galaxie version of NeoVim IDE Settings

It have a name because it existe, then **Mollasse** have been choise, it can be french understandable by something like ... 


Introduction
============
* Create a backup of you ``~/.config/nvim`` directory

.. code-block:: shell

	git clone https://gitlab.com/rtnp/galaxie-mollasse.git ~/.config/nvim
	nvim -c "autocmd User PackerComplete quitall" -c "PackerSync"
	nvim

Why
===
That is a strange idea to create a configuration for NeoVim, when it all ready existe a tonne of theme.

In fact `Mollasse` is my daily IDE setting, i use it everyday for profesional and personnal use. It focus on DevOps stack.
I share here my work in case that can help someone.

Features
========
- Full LSP
- Optimized for daily DevOps tasks
	* Python
  - Go
  - Ansible
  - Terraform
  - Dockerfile
  - Docker-compose
  - Sphinx
  - Rst
  - MarkDown
  - Json
- Darcula theme 
- File navigation with [nvim-tree.lua](https://github.com/kyazdani42/nvim-tree.lua).
- Managing tabs, buffers with [bufferline.nvim](https://github.com/akinsho/bufferline.nvim).
- Beautiful and configurable icons with [nvim-web-devicons](https://github.com/kyazdani42/nvim-web-devicons).
- Pretty and functional lualine with [lualine.nvim](https://github.com/nvim-lualine/lualine.nvim).
- Git diffs and more with [gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim) .
- NeoVim Lsp configuration with [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig).
- Autocompletion with [nvim-cmp](https://github.com/hrsh7th/nvim-cmp).
- File searching, previewing image and text files and more with [telescope.nvim](https://github.com/nvim-telescope/telescope.nvim).
- Syntax highlighting with [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter).
- Autoclosing braces and html tags with [nvim-autopairs](https://github.com/windwp/nvim-autopairs).
- Indentlines with [indent-blankline.nvim](https://github.com/lukas-reineke/indent-blankline.nvim).
- Useful snippets with [LuaSnip](https://github.com/L3MON4D3/LuaSnip).
- LazyGit / LazyDocker intergartion
- Terminal with floaterm 
 
Links
=====
- Vim Tips https://www.cs.swarthmore.edu/oldhelp/vim/home.html

