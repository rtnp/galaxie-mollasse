vim.opt.termguicolors = true

local bufferline_status_ok, bufferline = pcall(require, "bufferline")
if not bufferline_status_ok then
  return
end
local nord_status_ok, nord = pcall(require, "nord")
if not nord_status_ok then
  return
end

local highlights = nord.bufferline.highlights({
    italic = true,
    bold = true,
})

bufferline.setup({
    options = {
      numbers = "none", -- | "ordinal" | "buffer_id" | "both" | function({ ordinal, id, lower, raise }): string,
      close_command = "Bdelete! %d", -- can be a string | function, see "Mouse actions"
      right_mouse_command = "Bdelete! %d", -- can be a string | function, see "Mouse actions"
      left_mouse_command = "buffer %d", -- can be a string | function, see "Mouse actions"
      middle_mouse_command = nil, -- can be a string | function, see "Mouse actions"
      always_show_bufferline = true,
      separator_style = "thin",

    },
    highlights = highlights,
})
