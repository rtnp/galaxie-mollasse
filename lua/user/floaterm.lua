vim.g.floaterm_autoclose = 1
vim.g.floaterm_autoinsert = true
vim.g.floaterm_wintype = 'split'
vim.g.floaterm_position = 'botright'
vim.g.floaterm_title = 'Terminal: $1/$2'
vim.g.floaterm_rootmarkers  = { '.project', '.git', '.hg', '.svn', '.root' }
vim.g.floaterm_height  = 0.3
-- vim.g.floaterm_opener = 'tabe'


