local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
	return
end

configs.setup({
  markid = {
    enable = true,
    colors = false
  },
	ensure_installed = {
    "python",
    "lua",
    "vim",
    "help",
    "bash",
    "yaml",
    "go",
    "ini",
    "json",
    "markdown",
    "rst",
    "hcl",
    "terraform",
    "make",
  }, -- one of "all" or a list of languages
	ignore_install = { "" }, -- List of parsers to ignore installing
	highlight = {
		enable = true, -- false will disable the whole extension
		disable = { "" }, -- list of language that will be disabled
	},
	autopairs = {
		enable = true,
	},
	indent = {
    enable = true,
    disable = { "python"}
  },
  context_commentstring = {
    enable = true,
    enable_autocmd = false,
  },
  matchup = {
    enable = true
  }
})

local ufo_status_ok, ufo = pcall(require, "ufo")
if not ufo_status_ok then
	return
end

ufo.setup({
    provider_selector = function(bufnr, filetype, buftype)
        return {'treesitter', 'indent'}
    end
})
