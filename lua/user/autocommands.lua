  vim.api.nvim_create_autocmd({ "FileType" }, {
    pattern = {
      "Jaq",
      "qf",
      "help",
      "man",
      "lspinfo",
      "spectre_panel",
      "lir",
      "DressingSelect",
      "tsplayground",
    },
    callback = function()
      vim.cmd [[
      nnoremap <silent> <buffer> q :close<CR>
      set nobuflisted
    ]]
    end,
  })

vim.api.nvim_create_autocmd({ "TextYankPost" }, {
  pattern = "*",
  desc = "Highlight text on yank",
  callback = function()
    require("vim.highlight").on_yank { higroup = "Search", timeout = 100 }
  end,
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = "dap-repl",
  command = "set nobuflisted",
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = "qf",
  command = "set nobuflisted",
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = { "qf", "help", "man", "floaterm", "lspinfo", "lsp-installer", "null-ls-info" },
  command = "nnoremap <silent> <buffer> q :close<CR>",
})

vim.api.nvim_create_autocmd({ "VimResized" }, {
	callback = function()
		vim.cmd("tabdo wincmd =")
	end,
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = "alpha",
  callback = function()
    vim.cmd [[
      nnoremap <silent> <buffer> q :qa<CR>
      nnoremap <silent> <buffer> <esc> :qa<CR>
      set nobuflisted
    ]]
  end,
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = "lir",
  callback = function()
    vim.opt_local.number = false
    vim.opt_local.relativenumber = false
  end,
})

-- vim.api.nvim_create_autocmd("ColorScheme", {
--   pattern = "*",
--   callback = function()
--     local hl_groups = {
--       "Normal",
--       "SignColumn",
--       "NormalNC",
--       "TelescopeBorder",
--       "NvimTreeNormal",
--       "EndOfBuffer",
--       "MsgArea",
--     }
--     for _, name in ipairs(hl_groups) do
--       vim.cmd(string.format("highlight %s ctermbg=none guibg=none", name))
--     end
--   end,
-- })


vim.cmd("autocmd BufEnter * ++nested if winnr('$') == 1 && bufname() == 'NvimTree_' . tabpagenr() | quit | endif")

-- vim.api.nvim_create_autocmd({ "TextYankPost" }, {
-- 	callback = function()
-- 		vim.highlight.on_yank({ higroup = "Visual", timeout = 200 })
-- 	end,
-- })

vim.api.nvim_create_autocmd({ "BufWritePost" }, {
	pattern = { "*.java" },
	callback = function()
		vim.lsp.codelens.refresh()
	end,
})

vim.api.nvim_create_autocmd({ "VimEnter" }, {
	callback = function()
		vim.cmd("hi link illuminatedWord LspReferenceText")
	end,
})

vim.api.nvim_create_autocmd({ "BufWinEnter" }, {
	callback = function()
	local line_count = vim.api.nvim_buf_line_count(0)
		if line_count >= 5000 then
			vim.cmd("IlluminatePauseBuf")
		end
	end,
})
-- Don't show any numbers inside terminals
vim.cmd [[ au TermOpen term://* setlocal nonumber norelativenumber | setfiletype terminal ]]
-- Force ansible
vim.cmd[[ au BufRead *.yaml,*.yml if search('hosts:\|tasks:', 'nw') | set ft=yaml.ansible | endif ]]


-- vim.api.nvim_create_autocmd({ "VimEnter" }, {
-- 	callback = function()
--     vim.cmd [[highlight NvimTreeIndentMarker guifg=#D8DEE9]]
-- 	end,
-- })


