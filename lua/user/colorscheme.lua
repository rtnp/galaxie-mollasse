vim.g.nord_contrast = false
vim.g.nord_borders = true
vim.g.nord_disable_background = false
vim.g.nord_italic = true
vim.g.nord_uniform_diff_background = true
vim.g.nord_bold = false

local nord_status_ok, nord = pcall(require, "nord")
if not nord_status_ok then
	return
end
nord.set()
-- vim.cmd("highlight NvimTreeIndentMarker guifg=#D8DEE9")
