local status_ok, which_key = pcall(require, "which-key")
if not status_ok then
  return
end

local setup = {
  plugins = {
    marks = true, -- shows a list of your marks on ' and `
    registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
    spelling = {
      enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
      suggestions = 20, -- how many suggestions should be shown in the list?
    },
    -- the presets plugin, adds help for a bunch of default keybindings in Neovim
    -- No actual key bindings are created
    presets = {
      operators = true, -- adds help for operators like d, y, ... and registers them for motion / text object completion
      motions = true, -- adds help for motions
      text_objects = true, -- help for text objects triggered after entering an operator
      windows = true, -- default bindings on <c-w>
      nav = true, -- misc bindings to work with windows
      z = true, -- bindings for folds, spelling and others prefixed with z
      g = true, -- bindings for prefixed with g
    },
  },
  -- add operators that will trigger motion and text object completion
  -- to enable all native operators, set the preset / operators plugin above
  -- operators = { gc = "Comments" },
  key_labels = {
    -- override the label used to display some keys. It doesn't effect WK in any other way.
    -- For example:
    -- ["<space>"] = "SPC",
    -- ["<cr>"] = "RET",
    -- ["<tab>"] = "TAB",
  },
  icons = {
    breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
    separator = "➜", -- symbol used between a key and it's label
    group = "+", -- symbol prepended to a group
  },
  popup_mappings = {
    scroll_down = "<c-d>", -- binding to scroll down inside the popup
    scroll_up = "<c-u>", -- binding to scroll up inside the popup
  },
  window = {
    border = "none", -- none, single, double, shadow
    position = "bottom", -- bottom, top
    margin = { 1, 0, 1, 0 }, -- extra window margin [top, right, bottom, left]
    padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
    winblend = 0,
  },
  layout = {
    height = { min = 4, max = 25 }, -- min and max height of the columns
    width = { min = 20, max = 50 }, -- min and max width of the columns
    spacing = 3, -- spacing between columns
    align = "center", -- align columns left, center or right
  },
  ignore_missing = true, -- enable this to hide mappings for which you didn't specify a label
  hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ " }, -- hide mapping boilerplate
  show_help = true, -- show help message on the command line when the popup is visible
  triggers = "auto", -- automatically setup triggers
  -- triggers = {"<leader>"} -- or specify a list manually
  triggers_blacklist = {
    -- list of mode / prefixes that should never be hooked by WhichKey
    -- this is mostly relevant for key maps that start with a native binding
    -- most people should not need to change this
    i = { "j", "k" },
    v = { "j", "k" },
  },
}

local opts = {
  mode = "n", -- NORMAL mode
  prefix = "<leader>",
  buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
  silent = true, -- use `silent` when creating keymaps
  noremap = true, -- use `noremap` when creating keymaps
  nowait = true, -- use `nowait` when creating keymaps
}

local mappings = {
  ["a"] = { "<cmd>AerialToggle!<cr>", "Aerial Toggle" },
  ["A"] = { "<cmd>Alpha<cr>", "Alpha" },
  q = {":q<cr>", "Quit"},
  Q = {":q!<cr>", "Force Quit"},
  w = {":w<cr>", "Write"},
  b = {
    name = "Buffers",
    ["c"] = { "<cmd>Bdelete!<CR>", "Buffer Close" },
    [">"] = { "<cmd>BufferLineMoveNext<CR>", "Buffer Move Next" },
    ["<"] = { "<cmd>BufferLineMovePrev<CR>", "Buffer Move Prev" },
    ["."] = { "<cmd>BufferLineCycleNext<CR>", "Buffer Cycle Next" },
    [","] = { "<cmd>BufferLineCycleNext<CR>", "Buffer Cycle Prev" },
    C = {
      name = "Close",
      r = { "<Cmd>BufferLineCloseRight<CR>", "Buffer Close Right" },
      l = { "<Cmd>BufferLineCloseLeft<CR>", "Buffer Close Left" },
      p = { "<Cmd>BufferLinePickClose<CR>", "Buffer Pick Close" },
    },
    g = {
      name = "Groups",
      c = { "<Cmd>BufferLineGroupClose<CR>", "Buffer Group Close" },
      t = { "<Cmd>BufferLineGroupToggle<CR>", "Buffer Group Toggle" },
    },
    l = {
    "<cmd>lua require('telescope.builtin').buffers(require('telescope.themes').get_dropdown{previewer = false})<cr>",
    "Buffer List Dialog",
    },
    p = { "<Cmd>BufferLinePick<CR>", "Buffer Pick" },
    s = {
      name = "Sort By",
      e = { "<Cmd>BufferLineSortByExtension<CR>", "Extension" },
      d = { "<Cmd>BufferLineSortByDirectory<CR>", "Directory" },
      t = { "<Cmd>BufferLineSortByTabs<CR>", "Tabs" },

    },
    t = {
      name = "Go to",
      ["1"] = {"<cmd>lua require('bufferline').go_to_buffer(1, true)<cr>", "Buffer 1"},
      ["2"] = {"<cmd>lua require('bufferline').go_to_buffer(1, true)<cr>", "Buffer 2"},
      ["3"] = {"<cmd>lua require('bufferline').go_to_buffer(1, true)<cr>", "Buffer 3"},
      ["4"] = {"<cmd>lua require('bufferline').go_to_buffer(1, true)<cr>", "Buffer 4"},
      ["5"] = {"<cmd>lua require('bufferline').go_to_buffer(1, true)<cr>", "Buffer 5"},
      ["6"] = {"<cmd>lua require('bufferline').go_to_buffer(1, true)<cr>", "Buffer 6"},
      ["7"] = {"<cmd>lua require('bufferline').go_to_buffer(1, true)<cr>", "Buffer 7"},
      ["8"] = {"<cmd>lua require('bufferline').go_to_buffer(1, true)<cr>", "Buffer 8"},
      ["9"] = {"<cmd>lua require('bufferline').go_to_buffer(1, true)<cr>", "Buffer 9"},
      ["$"] = {"<cmd>lua require('bufferline').go_to_buffer(1, true)<cr>", "Buffer -1"},
    },
  },
  d = {
    name = "Debug",
    b = {"<cmd>lua require('dap').toggle_breakpoint()<cr>", "Toggle Breakpoint"},
    c = {"<cmd>lua require('dap').continue()<cr>", "Continue"},
    i = {"<cmd>lua require('dap').step_into()<cr>", "Step Intro"},
    o = {"<cmd>lua require('dap').step_over()<cr>", "Step Over"},
    O = {"<cmd>lua require('dap').step_out()<cr>", "Step Out"},
    r = {"<cmd>lua require('dap').repl.toggle()<cr>", "Replace Toggle"},
    l = {"<cmd>lua require('dap').run_last()<cr>", "Run Last"},
    u = {"<cmd>lua require('dapui').toggle()<cr>", "User Interface"},
    t = {"<cmd>lua require('dap').terminate()<cr>", "Terminate"},
  },
  ["e"] = { "<cmd>NvimTreeToggle<cr>", "File Explorer" },
  -- E = {":e ~/.config/nvim/lua/mollasse/user/init.lua<cr>", "Edit User Config"},
  ["h"] = { "<cmd>nohlsearch<CR>", "No Highlight" },
  ["F"] = { "<cmd>Telescope live_grep theme=ivy<cr>", "Find Text" },

  f = {
    name = "File",
    ["w"] = { "<cmd>w!<CR>", "File Save" },
    ["q"] = { "<cmd>q!<CR>", "File Quit" },
    ["r"] = { "<cmd>Telescope projects<cr>", "Open a Recent Project" },
  },
  p = {
    name = "Packer",
    r = {":PackerClean<cr>", "Remove Unused Plugins"},
    c = {":PackerCompile profile=true<cr>", "Recompile Plugins"},
    i = {":PackerInstall<cr>", "Install Plugins"},
    p = {":PackerProfile<cr>", "Packer Profile"},
    s = {":PackerSync<cr>", "Sync Plugins"},
    S = {":PackerStatus<cr>", "Packer Status"},
    u = {":PackerUpdate<cr>", "Update Plugins"}
  },
	-- keymap(bufnr, "n", "gl", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
 --  vim.cmd [[ command! Format execute 'lua vim.lsp.buf.formatting()' ]]
  l = {
    name = "LSP",
    i = {":LspInfo<cr>", "Connected Language Servers"},
    k = {'<cmd>lua vim.lsp.buf.signature_help()<CR>', "Signature help"},
    K = {'<cmd>Lspsaga hover_doc<cr>', "Hover"},
    w = {'<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', "Add workspace folder"},
    W = {'<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', "Remove workspace folder"},
    l = {
      '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>',
      "List workspace folder"
    },
    a = {'<cmd>Lspsaga code_action<CR>', "Code actions"},
    t = {'<cmd>TroubleToggle lsp_type_definitions<CR>', "Type definition"},
    d = {'<cmd>TroubleToggle lsp_definitions<CR>', "Go to definition"},
    D = {'<cmd>lua vim.lsp.buf.delaration()<CR>', "Go to declaration"},
    e = {'<cmd>Lspsaga show_line_diagnostics<cr>', "Show line diagnostics"},
    f = {'<cmd>lua vim.lsp.buf.format { async = true }<CR>', "Format File"},
    F = {'<cmd>Lspsaga lsp_finder<CR>', "LSP Finder"},
    r = {'<cmd>TroubleToggle lsp_references<CR>', "References"},
    R = {'<cmd>Lspsaga rename<CR>', "Rename"},
    n = {'<cmd>Lspsaga diagnostic_jump_next<CR>', "Go to next diagnostic"},
    N = {'<cmd>Lspsaga diagnostic_jump_prev<CR>', "Go to previous diagnostic"},
    p = {'<cmd>Lspsaga peek_definition<CR>', "Peek definition"},
    I = {'<cmd>LspInstallInfo<CR>', 'Install language server'},
    T = {'<cmd>TroubleToggle<CR>', "Get Diagnostics"}
  },
  r = {
    name = "Run",
    n = { "<cmd>lua require 'neotest'.run.run()<cr>", "Run the nearest test" },
    f = { "<cmd>lua require 'neotest'.run.run(vim.fn.expand('%'))<cr>", "Run the current file" },
    d = { "<cmd>lua require 'neotest'.run.run({strategy = 'dap'})<cr>", "Debug the nearest test" },
    s = { "<cmd>lua require 'neotest'.run.stop()<cr>", "Stop the nearest test" },
    a = { "<cmd>lua require 'neotest'.run.attach()<cr>", "Attach to the nearest test" },

    },
  s = {
    name = "Search",
    f = { "<cmd>Telescope find_files<cr>", "Files" },
    t = { "<cmd>Telescope live_grep<cr>", "Live Grep" },
    p = { "<cmd>Telescope projects<cr>", "Projects" },
    b = { "<cmd>Telescope buffers<cr>", "Buffers" },
    k = { "<cmd>Telescope keymaps<cr>", "Keymaps" },
    c = { "<cmd>Telescope commands<cr>", "Commands" },
  },
  t = {
    name = "Tools",
    t = {
      name = "Terminal",
      N = { "<cmd>FloatermNew<cr>", "Open a floaterm window" },
      p = { "<cmd>FloatermPrev<cr>", "Switch to the previous floaterm instance" },
      n = { "<cmd>FloatermNext<cr>", "Switch to the previous floaterm instance" },
      f = { "<cmd>FloatermFirst<cr>", "Switch to the first floaterm instance" },
      l = { "<cmd>FloatermLast<cr>", "Switch to the last floaterm instance" },
      h = { "<cmd>FloatermHide<cr>", "Hide the current floaterms window" },
      s = { "<cmd>FloatermShow<cr>", "Show the current floaterm window" },
      k = { "<cmd>FloatermKill<cr>", "Kill the current floaterm instance" },
      t = { "<cmd>FloatermToggle<cr>", "Open or hide the floaterm window" },
    },
    G = { "<cmd>FloatermNew --height=1.0 --width=1.0 --wintype=float --position=center --name=LazyGit --position=topleft --autoclose=1 lazygit<cr>", "LazyGit" },
    D = { "<cmd>FloatermNew --height=1.0 --width=1.0 --wintype=float --position=center --name=LazyDocker --position=topleft --autoclose=1 lazydocker<cr>", "LazyDocker" },
    N = { "<cmd>FloatermNew --height=1.0 --width=1.0 --wintype=float --position=center --name=LazyNPM --position=topleft --autoclose=1 lazynpm<cr>", "LazyNPM" },
    g = {
      name = "Git",
      j = { "<cmd>lua require 'gitsigns'.next_hunk()<cr>", "Next Hunk" },
      k = { "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", "Prev Hunk" },
      l = { "<cmd>lua require 'gitsigns'.blame_line()<cr>", "Blame" },
      p = { "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", "Preview Hunk" },
      r = { "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", "Reset Hunk" },
      R = { "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", "Reset Buffer" },
      s = { "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", "Stage Hunk" },
      u = { "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>","Undo Stage Hunk" },
      o = { "<cmd>Telescope git_status<cr>", "Open changed file" },
      b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
      c = { "<cmd>Telescope git_commits<cr>", "Checkout commit" },
      d = { "<cmd>Gitsigns diffthis HEAD<cr>", "Diff"},
    },
  },
  v = {
    name= "View",
    a = { "<cmd>AerialToggle!<cr>", "View Areial" },
    t = { "<cmd>FloatermToggle<cr>", "View Terminal" },
    q = { "<cmd>TroubleToggle quickfixe<cr>", "View Quick Fixe List" },
    l = { "<cmd>TroubleToggle loclist<cr>", "View Location List" },
    d = { "<cmd>TroubleToggle<cr>", "View Diagnostics" },
    D = {"<cmd>lua require('dapui').toggle()<cr>", "View Debuger"},

  },

  x = {":bdelete<cr>", "Close Buffer"},
  X = {":bdelete!<cr>", "Force Close Buffer"},
}

which_key.setup(setup)
which_key.register(mappings, opts)

